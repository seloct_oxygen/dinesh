package week3.day1classworks;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HandleDropdowns {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Question 1: Select source drop down in value in create lead form using visible text


		//Tell the chrome to where chromedriver is available

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");


		//Chromedriver object to access chrome

		ChromeDriver driver=new ChromeDriver();

		driver.manage().window().maximize();

		//To open the URL

		driver.get("http://leaftaps.com/opentaps");

		//enter the username

		driver.findElementById("username").sendKeys("DemoSalesManager");

		//enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		//To click login

		driver.findElementByClassName("decorativeSubmit").click();

		//To open crm/sfa

		driver.findElementByLinkText("CRM/SFA").click();

		// To open Create Lead

		driver.findElementByLinkText("Create Lead").click();

		//To enter company name

		WebElement Source = driver.findElementById("createLeadForm_dataSourceId");

		Select Value=new Select(Source);

		Value.selectByVisibleText("Direct Mail");

		//Question 2: Select a value in marketing drop down from the create lead using select by value


		WebElement Marketing = driver.findElementById("createLeadForm_marketingCampaignId");

		Select Value2=new Select(Marketing);

		Value2.selectByValue("CATRQ_AUTOMOBILE");
		
		//Question 3: Print a all values which start with c from the industry dropdown
		
		

		WebElement Industry = driver.findElementById("createLeadForm_industryEnumId");

		Select Value3=new Select(Industry);

		List<WebElement> options = Value3.getOptions();
		
		//To get size of the drop down
		
		System.out.println(options.size());
		
		for (WebElement Alloptions : options) {
			
			System.out.println("Each options in the dropdonw");
			System.out.println(Alloptions.getText());
			
			
		}
		
		for (WebElement Eachvalue : options) {
			
			if(Eachvalue.getText().startsWith("C")) {
				
				System.out.println(Eachvalue.getText());
			}
			
		}
		


	}




}
