package week3.day1classworks;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateLead {
	
	public static void main(String[] args) {
		//Tell the chrome to where chromedriver is available
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Chromedriver object to access chrome
		ChromeDriver driver=new ChromeDriver();
		//To open the URL
		driver.get("http://leaftaps.com/opentaps");
		//enter the username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter the password
		driver.findElementById("password").sendKeys("crmsfa");
		//To click login
		driver.findElementByClassName("decorativeSubmit").click();
		//To open crm/sfa
		driver.findElementByLinkText("CRM/SFA").click();
		// To open Create Lead
		driver.findElementByLinkText("Create Lead").click();
		//To enter company name
		driver.findElementById("createLeadForm_companyName").sendKeys("Edunex"); 
		//To enter First name
		driver.findElementById("createLeadForm_firstName").sendKeys("Roman ");
		// To enter last name
		driver.findElementById("createLeadForm_lastName").sendKeys("Reigns");
		// To click create lead
		driver.findElementByName("submitButton").click();
		driver.close();
	}
}
