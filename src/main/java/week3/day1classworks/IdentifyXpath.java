package week3.day1classworks;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class IdentifyXpath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Question : Open Find lead and verify the xpath for Firstname field and find leads button
		
		

	//Tell the chrome to where chromedriver is available
	
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	
	
	//Chromedriver object to access chrome
	
	ChromeDriver driver=new ChromeDriver();
	
	driver.manage().window().maximize();
	
	//To open the URL
	
	driver.get("http://leaftaps.com/opentaps");
	
	//enter the username
	
	driver.findElementById("username").sendKeys("DemoSalesManager");
	
	//enter the password
	
	driver.findElementById("password").sendKeys("crmsfa");
	
	//To click login
	
	driver.findElementByClassName("decorativeSubmit").click();
	
	//To open crm/sfa
	
	driver.findElementByLinkText("CRM/SFA").click();
	
	// To open leads
	
	driver.findElementByLinkText("Leads").click();
	
	
	// To open find leads
	
	driver.findElementByLinkText("Find Leads").click();
	
	// To place cursor on the first name field from the find leads
	
	driver.findElement(By.xpath("(//input[@name='firstName'])[3]")).click();
	
	//To click the find leads button using the Xpath
	
	
	driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();

}
	}