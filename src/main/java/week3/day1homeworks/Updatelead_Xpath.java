package week3.day1homeworks;

import org.openqa.selenium.chrome.ChromeDriver;

public class Updatelead_Xpath {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver updatelead=new ChromeDriver();
		updatelead.manage().window().maximize();
		//To open the URL
		updatelead.get("http://leaftaps.com/opentaps");
		updatelead.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		updatelead.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		updatelead.findElementByXPath("//input[@class='decorativeSubmit']").click();
		updatelead.findElementByXPath("//a[contains(text(), 'RM')]").click();
		updatelead.findElementByXPath("//a[text()='Leads']").click();
		updatelead.findElementByXPath("//a[text()='Find Leads']").click();
	    updatelead.findElementByXPath("(//input[contains(@class, 'text')])[29]").sendKeys("roman");
	    updatelead.findElementByXPath("//button[text()='Find Leads']").click();
	    Thread.sleep(2000);
	    updatelead.findElementByXPath("(//a[@class='linktext'])[4]").click();
	    Thread.sleep(2000);
	    updatelead.findElementByXPath("(//a[@class='subMenuButton'])[3]").click();
	    updatelead.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
	    updatelead.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("Edune");
	    String attribute = updatelead.findElementById("updateLeadForm_companyName").getAttribute("Value");
	    System.out.println(attribute);
	    updatelead.findElementByXPath("//input[@name='submitButton']").click();
	    String text = updatelead.findElementById("viewLead_companyName_sp").getText();
	    System.out.println(text);
	    if(text.contains(attribute)){
	    	System.out.println("company matched");
	    }else {
	    	System.out.println("company is not matched");
	    }
	    updatelead.close();
	}
}
