package week5.day1classworks;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	@Test
	public void report() throws IOException {
		//creating a blank html page
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		//Append as a single file
		html.setAppendExisting(true);
		//creating a class for extent reports
		ExtentReports extent=new ExtentReports();
		//attach the html to extent reports
		extent.attachReporter(html);
		//create test case name
		ExtentTest Test = extent.createTest("classwork", "reports with four pass and fail test cases");
		Test.assignAuthor("sandhiya");
		Test.assignCategory("smoke");
		Test.pass("step :1 pass", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		Test.pass("step :2 pass", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer1.png").build());
		Test.pass("step :3 pass", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		Test.pass("step :4 pass", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer1.png").build());
		Test.fail("step :5 failed", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		Test.fail("step :6 failed", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		Test.fail("step :7 failed", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		Test.fail("step :8 failed", MediaEntityBuilder.createScreenCaptureFromPath("./../screenshots/refer.png").build());
		extent.flush();
	}
}
//Question: To create test with four pass and four fail with extent reports