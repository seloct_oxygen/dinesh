package week5.day2homeworks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class ZoomwithSemethods extends SeMethods {
	
	@BeforeTest
	public void setData() {
		testcaseName ="TC006_zoomcar";
		testDesc ="Identify the carname";
		author ="Dinesh";
		category = "Sanity";
	}
	@Test
	public void zoomcarlist() {
		startApp("chrome", "https://www.zoomcar.com/chennai");
		click(locateElement("xpath", "//div[@class='newHomeSearch']"));
		click(locateElement("xpath", "//div[@class='component-popular-locations']/div[4]"));
		click(locateElement("class", "proceed"));
		click(locateElement("xpath", "//div[@class='days']/div[2]"));
		click(locateElement("class", "proceed"));
		verifyPartialText(locateElement("xpath", "//div[text()[contains(.,'Sat')]]"), "SAT");
		click(locateElement("class", "proceed"));
		List<WebElement> Results = driver.findElementsByXPath("//div[@class='price']");
        int size = Results.size();
        System.out.println("No of results :"+size);
        List<String> list = new ArrayList<>();
        for (WebElement ele : Results) {
       	 String price = ele.getText();
       	 String finalPrice = price.replaceAll("\\D", "");
       	 list.add(finalPrice);
		}
        Collections.sort(list,Collections.reverseOrder());
        /* String max = Collections.max(list);
        System.out.println(max);*/
        String amnt = list.get(0);
        System.out.println(amnt);
        if(driver.findElementByXPath("//div[text()[contains(.,'"+amnt+"')]]").getText().contains(amnt)){
        String text2 = driver.findElementByXPath("//div[text()[contains(., '"+amnt+"')]]/preceding::h3[1]").getText();
        driver.findElementByXPath("//div[contains(text(),'"+amnt+"')]/preceding::h3[1]");		
   	    System.out.println(text2);	
	}
}
}