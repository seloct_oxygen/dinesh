package week1.day2learnings;

public class LearnString_ex4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Another way declare the string in java
		String text = new String("TestLeaf"); 

		//used to identify the length of the string
		int size = text.length();
		System.out.println(size);

		//Used to merge two string value. Contact method
		text = text.concat("TestLeaf");
		System.out.println(text.concat(" TestLeaf"));


        //Used to change the string into upper case
		System.out.println(text.toUpperCase());
		
		
		//Used to remove the space front or beginning of the string
		System.out.println(text.trim());
		
		//Used to identify given char or string last occurrence in given string
		System.out.println(text.lastIndexOf("e"));

	}

}
