package week4.day2homeworks;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC005_MergeLead extends SeMethods {
	
	@Test
	public void MergeLead() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, "DemoSalesManager");
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, "crmsfa");
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleMergeLeadlink = locateElement("linktext", "Merge Leads");
	click(eleMergeLeadlink);
	String parentwindow = parentwindowHandle();
	WebElement eleFromLeadIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a");
	click(eleFromLeadIcon);	
	switchToWindow(1);
	WebElement eleFromId = locateElement("xpath", "//input[@name='id']");
	type(eleFromId, "10027");
	WebElement eleFleadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadsB);
	sleep();
	WebElement eleFromFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleFromFirstRecord);
	switchToParentWindow(parentwindow);
	sleep();
	WebElement eleToLeadIcon = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a");
	click(eleToLeadIcon);
	switchToWindow(1);
	WebElement eleToId = locateElement("xpath", "//input[@name='id']");
	type(eleToId, "10029");
	WebElement eleFLeadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFLeadsB);
	sleep();
	WebElement eleToFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleToFirstRecord);
	switchToParentWindow(parentwindow);
	sleep();
	WebElement eleMergeB = locateElement("linktext", "Merge");
	clickWithOutSnap(eleMergeB);
	acceptAlert();
	WebElement eleFleadsB3 = locateElement("linktext", "Find Leads");
	click(eleFleadsB3);
	WebElement eleLeadId = locateElement("xpath", "//input[@name='id']");
	type(eleLeadId, "10027");
	WebElement eleFleadB4 = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadB4);
	WebElement eleErrormsg = locateElement("class", "x-paging-info");
	verifyExactText(eleErrormsg, "No records to display");
	closeBrowser();	
	}
}
