package week6.day1classwork2;


import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import wdmethods.SeMethods;

public class ProjectMethods extends SeMethods {
	@BeforeMethod
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, "DemoCSR");
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, "crmsfa");
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	}
	@AfterMethod
	public void browserclose() {
		closeBrowser();
	}

}

