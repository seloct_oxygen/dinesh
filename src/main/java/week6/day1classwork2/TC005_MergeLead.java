package week6.day1classwork2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC005_MergeLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testcaseName ="Priority_ML";
		testDesc ="Merge a lead in leaftaps";
		author ="Dinesh";
		category = "Smoke";
	}
	@Test(priority=2)
	public void MergeLead() {
    //login();
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleMergeLeadlink = locateElement("linktext", "Merge Leads");
	click(eleMergeLeadlink);
	WebElement eleFromLeadIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a");
	click(eleFromLeadIcon);	
    String primarywindow=parentwindowHandle();
	//String primarywindow = driver.getWindowHandle();
	switchToWindow(1);
	WebElement eleFromId = locateElement("xpath", "//input[@name='id']");
	type(eleFromId, "10018");
	WebElement eleFleadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadsB);
	sleep();
	WebElement eleFromFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleFromFirstRecord);
	//switchToParentWindow(primarywindow);
	/*sleep();
	switchToWindow(0);*/
//	switchToWindow(0);
	driver.switchTo().window(primarywindow);
	sleep();
	WebElement eleToLeadIcon = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a");
	click(eleToLeadIcon);
	switchToWindow(1);
	WebElement eleToId = locateElement("xpath", "//input[@name='id']");
	type(eleToId, "10021");
	WebElement eleFLeadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFLeadsB);
	sleep();
	WebElement eleToFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleToFirstRecord);
	driver.switchTo().window(primarywindow);
	WebElement eleMergeB = locateElement("linktext", "Merge");
	clickWithOutSnap(eleMergeB);
	acceptAlert();
	WebElement eleFleadsB3 = locateElement("linktext", "Find Leads");
	click(eleFleadsB3);
	WebElement eleLeadId = locateElement("xpath", "//input[@name='id']");
	type(eleLeadId, "10018");
	WebElement eleFleadB4 = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadB4);
	WebElement eleErrormsg = locateElement("class", "x-paging-info");
	verifyExactText(eleErrormsg, "No records to display");
	//closeBrowser();	
	
	
	}

}
