package week6.day;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Stringsequence {
     static char ch='c';

	public static void main(String[] args) {

		System.out.println("Enter the input string");
		Scanner scan=new Scanner(System.in);
		String next = scan.next();
		System.out.println("input:"+next);
		char[] charArray = next.replace(" ", "").toCharArray();
		Set<Character> set=new LinkedHashSet<>();
		for (int i = 0; i < charArray.length; i++) {
			set.add(charArray[i]);
		}
		for (Character character : set) {
			System.out.print(character);
		}
	}		
}
