package week6.day;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class Sortnumberwithoutduplicates {
	
	public static void main(String[] args) {
		
		//int input[]= {1,8,6,5,5,3};
	//	char[] charArray = input.toString().toCharArray();
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the array size");
		int size =scan.nextInt();
		int[] arr=new int[size];
		for (int i = 0; i <size; i++) {
			arr[i]= scan.nextInt();
		}
		Set<Integer> removeduplicate=new HashSet<>();
		for (int i = 0; i < size; i++) {
			 removeduplicate.add(arr[i]);
			}
		List<Integer> allchar = new ArrayList<>();
		allchar.addAll(removeduplicate);
		Collections.sort(allchar);
		for (Integer numberwithoutduplicates : allchar) {
			System.out.println(numberwithoutduplicates);
		}
		
		}		
}
