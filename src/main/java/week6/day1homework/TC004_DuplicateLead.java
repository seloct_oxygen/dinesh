package week6.day1homework;

import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC004_DuplicateLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC004_DPL";
		testDesc ="Duplicate lead in leaftaps";
		author ="Dinesh";
		category = "Reg";
	}
	@Test(groups="reg",dataProvider="fetchdata",dependsOnGroups="sanity")
	public void DuplicateLead(String emailid) {
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleEmailTab = locateElement("linktext", "Email");
	click(eleEmailTab);
	WebElement eleEmailtextbox = locateElement("name", "emailAddress");
	type(eleEmailtextbox, emailid);
	WebElement eleFleadsB1 = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadsB1);
	sleep();
	WebElement eleFrecord = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
	click(eleFrecord);
	WebElement eleFirstname = locateElement("viewLead_firstName_sp");
	String text1 = getText(eleFirstname);
	WebElement eleDuplicateLeadB = locateElement("linktext", "Duplicate Lead");
	click(eleDuplicateLeadB);
	verifyTitle("Duplicate Lead | opentaps CRM");
	WebElement eleCreateLeadB = locateElement("class", "smallSubmit");
	click(eleCreateLeadB);
	WebElement eleFname = locateElement("viewLead_firstName_sp");
	String text2 = getText(eleFname);
	if (text1.equals(text2)) {
		System.out.println("name matched");
	} else {
		System.out.println("name does not matched");
	}	
}
	@DataProvider(name="fetchdata")
    public String[][] getdata(){
    	String[][] data=new String[1][1];
    	data[0][0]="anoe@gmail.com";
		return data;	
    }
}
