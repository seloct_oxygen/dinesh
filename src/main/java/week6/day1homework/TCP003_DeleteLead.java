package week6.day1homework;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TCP003_DeleteLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC003_DL";
		testDesc ="Delete a new lead in leaftaps";
		author ="Dinesh";
		category = "Sanity";
	}
	@Test(groups="sanity",dataProvider="fetchdata")
	public void DeleteLead(String phonecode,String phoneno) {
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement elePhonetab = locateElement("xpath", "//span[text()='Phone']");
	click(elePhonetab);
	WebElement elePnofield = locateElement("xpath", "//input[@name='phoneAreaCode']");
	type(elePnofield, phonecode);
	WebElement elePhoneno = locateElement("xpath", "//input[@name='phoneNumber']");
	type(elePhoneno, phoneno);
	WebElement eleFindlead = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindlead);
	sleep();
	WebElement eleText1 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	String ResultingId= getText(eleText1);
	click(eleText1);
	WebElement eleDelete = locateElement("class", "subMenuButtonDangerous");
	click(eleDelete);
	WebElement eleFindleadB = locateElement("linktext", "Find Leads");
	click(eleFindleadB);
	WebElement eleIdfield = locateElement("xpath", "//input[@name='id']");
	eleIdfield.toString();
	type(eleIdfield, ResultingId);
	WebElement elefleadV = locateElement("xpath", "//button[text()='Find Leads']");
	click(elefleadV);
	WebElement eleText2 = locateElement("class", "x-paging-info");
	verifyExactText(eleText2, "No records to display");
}
	@DataProvider(name="fetchdata")
    public String[][] getdata(){
    	String[][] data=new String[1][2];
    	data[0][0]="91";
    	data[0][1]="5454545444";
		return data;	
    }
}
