package week6.day1homework;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TCP005_MergeLead extends ProjectMethods {
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC005_DPL";
		testDesc ="Merge lead in leaftaps";
		author ="Dinesh";
		category = "Smoke";
	}
	@Test(dataProvider="fetchdata")
	public void MergeLead(String fromId,String toId) {
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleMergeLeadlink = locateElement("linktext", "Merge Leads");
	click(eleMergeLeadlink);
	String parentwindow = parentwindowHandle();
	WebElement eleFromLeadIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a");
	click(eleFromLeadIcon);	
	switchToWindow(1);
	WebElement eleFromId = locateElement("xpath", "//input[@name='firstName']");
	type(eleFromId, fromId);
	WebElement eleFleadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadsB);
	sleep();
	String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
	WebElement eleFromFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleFromFirstRecord);
	switchToParentWindow(parentwindow);
	sleep();
	WebElement eleToLeadIcon = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a");
	click(eleToLeadIcon);
	switchToWindow(1);
	WebElement eleToId = locateElement("xpath", "//input[@name='firstName']");
	type(eleToId, toId);
	WebElement eleFLeadsB = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFLeadsB);
	sleep();
	WebElement eleToFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithOutSnap(eleToFirstRecord);
	switchToParentWindow(parentwindow);
	sleep();
	WebElement eleMergeB = locateElement("linktext", "Merge");
	clickWithOutSnap(eleMergeB);
	acceptAlert();
	WebElement eleFleadsB3 = locateElement("linktext", "Find Leads");
	click(eleFleadsB3);
	WebElement eleLeadId = locateElement("xpath", "//input[@name='id']");
	type(eleLeadId, text);
	WebElement eleFleadB4 = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadB4);
	WebElement eleErrormsg = locateElement("class", "x-paging-info");
	verifyExactText(eleErrormsg, "No records to display");
	}
	@DataProvider(name="fetchdata")
    public String[][] getdata(){
    	String[][] data=new String[1][2];
    	data[0][0]="Ananth1";
    	data[0][1]="Dinesh";
		return data;	
    }
}
