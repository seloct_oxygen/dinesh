package week6.day1homework;


import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;

public class ProjectMethods extends SeMethods {
	@Parameters({"browser","url","uname","pwd"})
	@BeforeMethod(groups="config")
	public void login(String browser,String url,String uname,String pwd) {
	startApp(browser, url);
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, uname);
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, pwd);
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	}
	
	@AfterMethod(groups="config")
	public void browserclose() {
		closeBrowser();
	}

}

