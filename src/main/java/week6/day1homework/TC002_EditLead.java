package week6.day1homework;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC002_EditLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC002_EL";
		testDesc ="Edit a lead in leaftaps";
		author ="Dinesh";
		category = "Sanity";
	}
	@Test(groups="sanity",dataProvider="fetchdata",dependsOnGroups="smoke")
	public void editlead(String fname,String cname) {
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleFname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
	type(eleFname, fname);
	WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindLeads);
	sleep();
	WebElement eleFvalue = locateElement("xpath", "(//a[@class='linktext'])[4]");
	click(eleFvalue);
	sleep();
	WebElement eleEditLead = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
	click(eleEditLead);
	WebElement eleCmpnameclr = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	clear(eleCmpnameclr);
	WebElement eleCompname = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	type(eleCompname, cname);
	WebElement eleSubmit = locateElement("xpath", "//input[@name='submitButton']");
	click(eleSubmit);
	WebElement eleCname = locateElement("id", "viewLead_companyName_sp");
	String text = getText(eleCname);
	String replaceAll = text.replaceAll("//D", "");
	System.out.println(replaceAll);
	//Need to confirm the changed name appears
}
	@DataProvider(name="fetchdata")
    public String[][] getdata(){
    	String[][] data=new String[1][2];
    	data[0][0]="Dinesh";
    	data[0][1]="TCS";
		return data;	
    }
    }

