package week6.day2classwork2;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;

public class ProjectMethods extends SeMethods {
	
	@DataProvider(name = "fetchdata")
	public String[][] getdata() throws IOException {
	    return ReadExcel.getexcelname(excelname);
	}
	
	
	@Parameters({"browser","url","uname","pwd"})
	@BeforeMethod(groups="config")
	public void login(String browser,String url,String uname,String pwd) {
	startApp(browser, url);
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, uname);
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, pwd);
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	}
	
	@AfterMethod(groups="config")
	public void browserclose() {
		closeBrowser();
	}

}

