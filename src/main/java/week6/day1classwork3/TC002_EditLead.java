package week6.day1classwork3;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
//Question: Create login steps as method with before method and remove the login steps from all test cases
import org.testng.annotations.Test;


public class TC002_EditLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC002_EL";
		testDesc ="Edit a lead in leaftaps";
		author ="Dinesh";
		category = "Smoke";
	}
	//@Test(priority=4)
	@Test(groups="reg")
	public void editlead() {
	//login();	
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleFname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
	type(eleFname, "joe1");
	WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindLeads);
	sleep();
	WebElement eleFvalue = locateElement("xpath", "(//a[@class='linktext'])[4]");
	click(eleFvalue);
	sleep();
	WebElement eleEditLead = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
	click(eleEditLead);
	WebElement eleCmpnameclr = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	clear(eleCmpnameclr);
	WebElement eleCompname = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	type(eleCompname, "Raw");
	WebElement eleSubmit = locateElement("xpath", "//input[@name='submitButton']");
	click(eleSubmit);
	//Need to confirm the changed name appears
	//closeBrowser();
}
}
