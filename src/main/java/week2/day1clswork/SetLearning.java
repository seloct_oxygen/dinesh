package week2.day1clswork;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


public class SetLearning {

	public static void main(String[] args) {
		List<Long> numb=new ArrayList<>();
		 numb.add(1234456889766l);
		 numb.add(1234456889766l);
		 numb.add(1234456855766l);
		 numb.add(1234456689766l);
		 numb.add(1234456889766l); 
		 Set<Long> set = new LinkedHashSet<>();
		 set.addAll(numb);
		 for (Long long1 : set) {
			System.out.println(long1);
		}
		// System.out.println(set); 
	}
	
	
}
/*Question: To write a program to remove the duplicate from the list using the set.
Note: Set will not allow the duplicate values.*/