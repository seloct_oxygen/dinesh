package week7.day;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//open book
		XSSFWorkbook mybook=new XSSFWorkbook("./data/learnExcel.xlsx");
		//pick the sheet
		XSSFSheet sheet = mybook.getSheet("Sheet1");
		//getting the row count that has data
		int rowcount = sheet.getLastRowNum();
		System.out.println(rowcount);
		//getting the col count that has data using the row start point
		int colcount=sheet.getRow(0).getLastCellNum();
		System.out.println(colcount);
	
		for (int i = 0; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			{
		for (int j = 0; j < colcount; j++) {
			XSSFCell cell = row.getCell(j);
			String stringCellValue = cell.getStringCellValue();
			System.out.println(stringCellValue);
		}
	}
		}
	}
	
}

		