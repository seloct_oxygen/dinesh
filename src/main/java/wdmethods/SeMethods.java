package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.errorprone.annotations.ForOverride;

public class SeMethods extends Reporter implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			}else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser "+browser+" launched successfully"); 
			reportSteps("pass", "The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			System.err.println("The browser "+browser+" launched successfully");
			reportSteps("fail", "The browser "+browser+" launched successfully");
		}finally {
			takeSnap();
		}
		
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "tagname": return driver.findElementByTagName(locValue);
			case "paritallinktext": return driver.findElementByPartialLinkText(locValue);
			case "cssselector": return driver.findElementByCssSelector(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
			reportSteps("pass", "The element is not found");
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
			reportSteps("pass", "The element is not found");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		}catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
	}
		return null;
	}
	@Override
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementsById(locValue);
			case "name": return driver.findElementsByName(locValue);	
			case "xpath": return driver.findElementsByXPath(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "linktext": return driver.findElementsByLinkText(locValue);
			case "tagname": return driver.findElementsByTagName(locValue);
			case "partiallinktext": return driver.findElementsByPartialLinkText(locValue);
			case "cssselector": return driver.findElementsByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;
	}
	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			//System.out.println("The data "+data+" enter successfully");
			reportSteps("pass", "The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			//System.out.println("The data "+data+" not enter successfully");
			reportSteps("fail", "The data "+data+" not enter successfully");
			
		} finally {
			takeSnap();
		}
	}
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The element "+ele+" clicked");
			reportSteps("pass", "The element "+ele+" clicked");
		} catch (WebDriverException e) {
			//System.out.println("The element "+ele+" is not clicked");
			reportSteps("fail", "The element "+ele+" not clicked");
		} finally {
			takeSnap();
		}	
	}
	public void clickWithOutSnap(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The element "+ele+" clicked");
			reportSteps("pass", "The element "+ele+" clicked");
		} catch (WebDriverException e) {
			//System.out.println("The element "+ele+" is not clicked");
			reportSteps("fail", "The element "+ele+" not clicked");	
		} 
	}
	@Override
	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
			//System.out.println("The text "+ele+" is captured successfully");
			reportSteps("pass", "The text "+ele+" is captured successfully");
			return text;
		} catch (NoSuchElementException e) {
			//System.out.println("The text "+ele+" is not captured");
			reportSteps("fail", "The text "+ele+" is not captured");
		}
		return null;
	}
	@Override
	public void selectDropDown(WebElement ele, String value, String sel) {
		try {
			Select dropdown = new Select(ele);
			if(sel.equalsIgnoreCase("visible")) {
				dropdown.selectByVisibleText(value);
			}else if (sel.equalsIgnoreCase("value")) {
				dropdown.selectByValue(value);
			} else if (sel.equalsIgnoreCase("index")) {
				dropdown.selectByIndex(Integer.parseInt(value)); 
			}
			//System.out.println("The dropdown "+ele+" is selected");
			reportSteps("pass", "The dropdown "+ele+" is selected");
		} catch (NoSuchElementException e) {
			//System.out.println("The dropdown "+ele+" is not selected");
			reportSteps("fail", "The dropdown "+ele+" is not selected");
		} finally {
			takeSnap();
		}
	}
	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
	}
	@Override
	public void selectDropDownUsingValue(WebElement ele, int index) {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn = false;
		try {
			String title = driver.getTitle();
			if(title.equals(expectedTitle)) {
				System.out.println("The Title is matched with "+expectedTitle);
				bReturn=true;
			} else {
				System.out.println("The Title is not matched with "+expectedTitle);
			}
			//System.out.println("Title verified successfully");
			reportSteps("pass", "Title verified successfully");
		} catch (Exception e) {
			//System.out.println("Webpage is not available to verify the title");
			reportSteps("warning", "Title is not verified successfully");
		}
		return bReturn;
	}
	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
       try {
		if(ele.getText().equals(expectedText)) {
			   System.out.println("The text matched");
		   } else {
			   System.out.println("The text is not matched");
		   }
		//System.out.println("verified exact text successfully");
		reportSteps("pass", "verified exact text successfully");
	} catch (Exception e) {
		//System.out.println("Webpage is not available to verify the exact text");
		reportSteps("warning", "verified exact text not successfully");
	} finally {
		takeSnap();
	}
	}
	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().contains(expectedText)) {
				   System.out.println("The text matched");
			   } else {
				   System.out.println("The text not matched");
			   }
			//System.out.println("verified partial text successfully");
			reportSteps("pass", "verified partial text successfully");
		} catch (Exception e) {
			//System.out.println("webpage is not available to verify the partial text");
			reportSteps("warning", "verified partial text not successfully");
		} finally {
			takeSnap();
		}
	}
	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
         try {
			if(ele.getAttribute(attribute).equals(value)) {
				 System.out.println("The attribute text matched");
			 }else {
				 System.out.println("The attribute text not matched");
			 }
			//System.out.println("verified ExactAttribute successfully");
			reportSteps("pass", "verified ExactAttribute successfully");
		} catch (Exception e) {
			//System.out.println("webpage is not available to verify the exact attribute");
			reportSteps("warning", "verified ExactAttribute not successfully");
		}
	}
	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).contains(value)) {
			 System.out.println("The attribute text matched");
			}else {
			 System.out.println("The attribute text not matched");
			}
			//System.out.println("verified partial attribute successfully");
			reportSteps("pass", "verified partial attribute successfully");
		} catch (Exception e) {
			//System.out.println("Webpage is not available to verify the partial attribute");
			reportSteps("warning", "verified partial attribute not successfully");
		}
	}
	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				System.out.println("The element "+ele+" is selected");
			}else {
				System.out.println("The element "+ele+" is not selected");
			}
			//System.out.println("verified selected "+ele+"successfully");
			reportSteps("pass", "verified Isselected "+ele+" successfully");
		} catch (NoSuchElementException e) {
			//System.out.println("The element is not found");
			reportSteps("fail", "verified selected "+ele+" not successfully");
		}
	}
	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				System.out.println("The element "+ele+" is displayed");
			}else {
				System.out.println("The element "+ele+" is not displayed");
			}
			//System.out.println("verified displayed "+ele+"successfully");
			reportSteps("pass", "verified displayed "+ele+" successfully");
		} catch (NoSuchElementException e) {
			//System.out.println("The element is not found");
			reportSteps("fail", "verified displayed "+ele+" not successfully");
		} finally {
			takeSnap();
		}
	}
	@Override
	public void verifyEnabled(WebElement ele) {
		try {
			if(ele.isEnabled()) {
				System.out.println("The element "+ele+" is enabled");
			}else {
				System.out.println("The element "+ele+" is not displayed");
			}
			//System.out.println("verified enabled "+ele+"successfully");
			reportSteps("pass", "verified enabled "+ele+" successfully");
		} catch (NoSuchElementException e) {
			//System.out.println("The element is not found");
			reportSteps("fail", "verified enabled "+ele+" not successfully");
		} finally {
			takeSnap();
		}
	}
	@Override
	public void switchToWindow(int index) {
      try {
		  Set<String> allWindows = driver.getWindowHandles();
		  List<String> list = new ArrayList<>();
		  list.addAll(allWindows);
		  driver.switchTo().window(list.get(index));
		  //System.out.println("Switched to window successfully");
		  reportSteps("pass", "Switched to window successfully");
	} catch (NoSuchWindowException e) {
		//System.out.println("This is not an window");
		reportSteps("fail", "Switched to window not successfully");
	}
	}
	@Override
	public void switchToParentWindow(String Primarywindow) {
		try {
			//String Primarywindow = driver.getWindowHandle();
			driver.switchTo().window(Primarywindow);
			//System.out.println("Switched to primary window successfully");
			reportSteps("pass", "Switched to parent window successfully");
		} catch (NoSuchWindowException e) {
			//System.out.println("window is not present");
			reportSteps("fail", "Switched to parent window not successfully");
		}
	}
	@Override
	public String parentwindowHandle() {
		try {
			String windowHandle = driver.getWindowHandle();
			//System.out.println("parent window captured successfully");
			reportSteps("pass", "parent window captured successfully");
			return windowHandle;
		} catch (NoSuchWindowException e) {
			//System.out.println("window is not present");
			reportSteps("fail", "parent window not captured successfully");
		}
		return null;
	}
	@Override
	public void switchToFrameByElement(WebElement ele) {
       try {
		driver.switchTo().frame(ele);
		//System.out.println("Switched to frame successfully");
		reportSteps("pass", "Switched to frame successfully");
	} catch (NoSuchFrameException e) {
		//System.out.println("This is not a frame");
		reportSteps("fail", "Switched to frame not successfully");
	}
	}
	@Override
	public void switchToFrameByIndex(int value) {
		try {
			driver.switchTo().frame(value);
			//System.out.println("Switched to frame successfully");
			reportSteps("pass", "Switched to frame successfully");
		} catch (NoSuchFrameException e) {
			//System.out.println("This is not a frame");
			reportSteps("fail", "Switched to frame not successfully");
		}
	}
	@Override
	public void switchToFrameByIdorName(String value) {
		try {
			driver.switchTo().frame(value);
			//System.out.println("This is not a frame");
			reportSteps("pass", "Switched to frame successfully");
		} catch (NoSuchFrameException e) {
			//System.out.println("This is not a frame");
			reportSteps("fail", "Switched to frame not successfully");
		}	
	}
	@Override
	public void switchToParentFrame() {
		try {
			driver.switchTo().parentFrame();
			//System.out.println("Switched to parent frame successfully");
			reportSteps("pass", "Switched to parent frame successfully");
		} catch (NoSuchFrameException e) {
			//System.out.println("Frame is not switched parent frame");
			reportSteps("fail", "Switched to parent frame not successfully");
		}
	}
	@Override
	public void exitFrame() {
		try {
			driver.switchTo().defaultContent();
			//System.out.println("come out of frame successfully");
			reportSteps("pass", "Frame exit successfully");
		} catch (NoSuchFrameException e) {
			reportSteps("fail", "Frame exit not successfully");
		}
	}
	@Override
	public void acceptAlert() {
     try {
		driver.switchTo().alert().accept();
		//System.out.println("Alert accepted");
		reportSteps("pass", "Alert accepted");
	} catch (NoAlertPresentException e) {
		//System.out.println("Alert is not present");
		reportSteps("fail", "Alert not accepted");
	} 
	}
	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			//System.out.println("Alert cancelled");
			reportSteps("pass", "Alert cancelled");
		} catch (NoAlertPresentException e) {
			//System.out.println("Alert is not prsent");
			reportSteps("fail", "Alert not cancelled");
		}
	}
	@Override
	public void EntervalueAlert(String value) {
		try {
			Alert alert = driver.switchTo().alert();
			alert.sendKeys(value);
			//System.out.println("Alert +value+ entered successfully");
			reportSteps("pass", "Alert +value+ entered successfully");
		} catch (NoAlertPresentException e) {
			//System.out.println("Alert is not present");
			reportSteps("fail", "Alert +value+ not entered successfully");
		} 
	}
	@Override
	public String getAlertText() {
		try {
			driver.switchTo().alert().getText();
			//System.out.println("Alert text read successfully");
			reportSteps("pass", "Alert text read successfully");
		} catch (NoAlertPresentException e) {
			//System.out.println("Alert is not present");	
			reportSteps("fail", "Alert text not read successfully");
		}
		return null;
	}
	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
}
	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		try {
			driver.close();
			//System.out.println("current window closed successfully");
			reportSteps("pass", "current window closed successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.out.println("Unknown exception while closing the browser");
			reportSteps("fail", "current window is not closed");
		}
	}
	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		try {
			driver.quit();
			//System.out.println("All windows is closed successfully");
			reportSteps("pass", "All windows is closed successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.out.println("Unknown exception while closing the allbrowser");
			reportSteps("fail	", "All windows is not closed successfully");
		}
	}
	@Override
	public void sleep() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void webdriverwait(WebElement ele) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
		
	}
	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
		} catch (WebDriverException e) {
			System.out.println("Unkown execption");
		}
	}
}
		
	



	
